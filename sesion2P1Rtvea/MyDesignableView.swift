//
//  MyDesignableView.swift
//  rtve
//
//  Created by Pako on 6/11/17.
//  Copyright © 2017 svq.ventures. All rights reserved.
//

import UIKit

@IBDesignable class MyDesignableView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            layer.borderWidth = borderWidth > 0 ? borderWidth : 0
        }
    }
    @IBInspectable var borderColor: UIColor?{
        get{
            return UIColor(cgColor: layer.borderColor!)
        }
        set{
            layer.borderColor = newValue?.cgColor
        }
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
