//
//  ViewController.swift
//  rtve
//
//  Created by Pako on 6/11/17.
//  Copyright © 2017 svq.ventures. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var products: [String] = ["hat","bow","tie"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        self.tableView.delegate = self
//        self.tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- UITableView datasource delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath)
        cell.textLabel?.text = products[indexPath.row]
        return cell
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        performSegue(withIdentifier: "basicCellSegue", sender: self)
//    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "basicCellSegue", let destinationVC = segue.destination as? DetailProductViewController{
            if let indexPath = tableView.indexPathForSelectedRow
            {
                destinationVC.product = products[indexPath.row]
                //tabBarItem.title = "some"
                let icon = UIImage(named: "target")?.withRenderingMode(.alwaysOriginal)
                let tabBarIcon = UITabBarItem(title: "some", image: icon, selectedImage: icon)
                destinationVC.tabBarItem = tabBarIcon
            }
        }
    }
    @IBAction func unwindWithSelectedProduct(segue: UIStoryboardSegue){
        if let detailPVC = segue.source as? DetailProductViewController{
            let productShown = detailPVC.product
            print(#function, productShown!)
        }
    }
}

